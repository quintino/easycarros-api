import { ApiImplicitParam } from '@nestjs/swagger';
import { AppService } from './app.service';
import { Controller, Get, Res, HttpStatus, Post, Body } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { Response } from 'express';
import { createClient } from '@google/maps';

const privateKey = 'privateKey2019!';

@Controller()
export class AppController {

    constructor(
        private readonly service: AppService,
    ) {}

    @Get('allUsers')
    async listUsers(@Res() response: Response) {
        const value = await this.service.allUsers();
        response.status(HttpStatus.OK).json(value);
    }

    @Get('allPartners')
    async listPartners(@Res() response: Response) {
        const value = await this.service.allPartners();
        response.status(HttpStatus.OK).json(value);
    }

    @Get('allAddresses')
    async listAdresses(@Res() response: Response) {
        const value = await this.service.allAddresses();
        response.status(HttpStatus.OK).json(value);
    }

    @Post('login')
    @ApiImplicitParam({ name: 'password' })
    @ApiImplicitParam({ name: 'email' })
    async login(@Body() data: any, @Res() response: Response) {
        const value = await this.service.login(data.email, data.password);
        if (value === null || value === undefined) {
            response.status(HttpStatus.NOT_FOUND).json({ user: 'not_found' });
        } else {
            response.status(HttpStatus.OK).json({ access_token: jwt.sign(JSON.stringify(value), privateKey) });
        }
    }

    @Post('verify')
    @ApiImplicitParam({ name: 'token' })
    async verify(@Body() data: any, @Res() response: Response) {
        try {
            jwt.verify(data.token, privateKey);
            response.status(HttpStatus.OK).json({ token: 'valid' });
        } catch {
            response.status(HttpStatus.UNAUTHORIZED).json({ token: 'invalid' });
        }
    }

    @Post('findOne')
    @ApiImplicitParam({ name: 'token' })
    @ApiImplicitParam({ name: 'service' })
    @ApiImplicitParam({ name: 'lat', type: Number })
    @ApiImplicitParam({ name: 'long', type: Number })
    async search(@Body() data: any, @Res() response: Response) {
        try {
            jwt.verify(data.token, privateKey);
            const value = await this.service.search(data.service, data.lat, data.long);
            if (value === null || value === undefined) {
                response.status(HttpStatus.NOT_FOUND).json({ partner: 'not_found' });
            } else {
                response.status(HttpStatus.OK).json(value);
            }
        } catch (error) {
            response.status(HttpStatus.UNAUTHORIZED).json({ token: 'invalid' });
        }
    }

    @Post('findPartners')
    @ApiImplicitParam({ name: 'token' })
    @ApiImplicitParam({ name: 'address' })
    async findPartners(@Body() data: any, @Res() response: Response) {
        try {
            jwt.verify(data.token, privateKey);
            const map = createClient({ key: 'AIzaSyCzXRTN0KGItHp8vcss5wTNfYDYSXAPbro', Promise });
            map.geocode({ address: data.address }).asPromise().then(resp => {
                const loc = resp.json.results[0].geometry.location;
                this.service.searchPartners(loc.lat + '', loc.lng + '').then(value => {
                    response.status(HttpStatus.OK).json(value);
                }).catch(() => {
                    response.status(HttpStatus.OK).json([]);
                });
            }).catch(() => {
                response.status(HttpStatus.NOT_FOUND).json({ address: 'not_found' });
            });
        } catch (error) {
            response.status(HttpStatus.UNAUTHORIZED).json({ token: 'invalid' });
        }
    }

}
