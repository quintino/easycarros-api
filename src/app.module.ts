import { AddressSchema } from './schemas/address.schema';
import { PartnerSchema } from './schemas/partner.schema';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './schemas/user.schema';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb://easy:carros123@ds229088.mlab.com:29088/easycarros',
      { useNewUrlParser: true, useUnifiedTopology: true },
    ),
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Address', schema: AddressSchema }]),
    MongooseModule.forFeature([{ name: 'Partner', schema: PartnerSchema }]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
