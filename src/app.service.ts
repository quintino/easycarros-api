import { InjectModel } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';
import { User } from './interfaces/user.interface';
import { Model } from 'mongoose';
import { Partner } from './interfaces/partner.interface';
import { Address } from './interfaces/address.interface';

const distance = 0.067305999415;

@Injectable()
export class AppService {

    constructor(
        @InjectModel('User') private readonly userModel: Model<User>,
        @InjectModel('Address') private readonly addressModel: Model<Address>,
        @InjectModel('Partner') private readonly partnerModel: Model<Partner>,
    ) {}

    async login(email: string, password: string): Promise<User> {
        return await this.userModel
                            .where('email').eq(email)
                            .where('password').eq(password)
                            .findOne().exec();
    }

    async allUsers(): Promise<User[]> {
        return await this.userModel.find().exec();
    }

    async allPartners(): Promise<Partner[]> {
        return await this.partnerModel.find().exec();
    }

    async allAddresses(): Promise<Address[]> {
        return await this.addressModel.find().exec();
    }

    private calculateDistance(value: string, type: boolean = false): number {
        if (type) {
            return parseFloat(value) - (distance / 2);
        } else {
            return parseFloat(value) + (distance / 2);
        }
    }

    async search(service: string, lat: string, lon: string): Promise<Partner> {
        let latGT = 0;
        let latLT = 0;
        let lonGT = 0;
        let lonLT = 0;
        if (parseFloat(lat) < 0) {
            latGT = this.calculateDistance(lat, true);
            latLT = this.calculateDistance(lat);
        } else {
            latGT = this.calculateDistance(lat);
            latLT = this.calculateDistance(lat, true);
        }
        if (parseFloat(lon) < 0) {
            lonGT = this.calculateDistance(lon, true);
            lonLT = this.calculateDistance(lon);
        } else {
            lonGT = this.calculateDistance(lon);
            lonLT = this.calculateDistance(lon, true);
        }
        return await this.partnerModel
                            .where('availableServices').eq(service)
                            .where('location.lat').gt(latGT).lt(latLT)
                            .where('location.long').gt(lonGT).lt(lonLT)
                            .findOne().exec();
    }

    async searchPartners(lat: string, lon: string): Promise<Partner> {
        let latGT = 0;
        let latLT = 0;
        let lonGT = 0;
        let lonLT = 0;
        if (parseFloat(lat) < 0) {
            latGT = this.calculateDistance(lat, true);
            latLT = this.calculateDistance(lat);
        } else {
            latGT = this.calculateDistance(lat);
            latLT = this.calculateDistance(lat, true);
        }
        if (parseFloat(lon) < 0) {
            lonGT = this.calculateDistance(lon, true);
            lonLT = this.calculateDistance(lon);
        } else {
            lonGT = this.calculateDistance(lon);
            lonLT = this.calculateDistance(lon, true);
        }
        return await this.partnerModel
                        .where('location.lat').gt(latGT).lt(latLT)
                        .where('location.long').gt(lonGT).lt(lonLT)
                        .find().exec();
    }

}
