import { Document } from 'mongoose';

export interface Address extends Document {
    readonly name: string;
    readonly address: string;
    readonly city: string;
    readonly state: string;
    readonly country: string;
    readonly lat: number;
    readonly long: number;
}
