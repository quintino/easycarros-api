import { Address } from './address.interface';
import { Document } from 'mongoose';

export interface Partner extends Document {
    readonly id: string;
    readonly name: string;
    readonly availableServices: string[];
    readonly location: Address;
}
