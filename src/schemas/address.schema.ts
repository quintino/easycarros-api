import * as mongoose from 'mongoose';

export const AddressSchema = new mongoose.Schema({
    name: String,
    address: String,
    city: String,
    state: String,
    country: String,
    lat: Number,
    long: Number,
});
