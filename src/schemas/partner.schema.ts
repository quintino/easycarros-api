import { AddressSchema } from './address.schema';
import * as mongoose from 'mongoose';

export const PartnerSchema = new mongoose.Schema({
  id: String,
  name: String,
  availableServices: [String],
  location: AddressSchema,
});
